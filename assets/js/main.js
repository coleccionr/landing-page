/*
	Polymorph by Pixelarity
	pixelarity.com | hello@pixelarity.com
	License: pixelarity.com/license
*/

(function($) {

	skel.breakpoints({
		xlarge:	'(max-width: 1680px)',
		large:	'(max-width: 1280px)',
		medium:	'(max-width: 980px)',
		small:	'(max-width: 736px)',
		xsmall:	'(max-width: 480px)',
		xxsmall: '(max-width: 360px)'
	});

	$(function() {

		var	$window = $(window),
			$body = $('body'),
			$header = $('#header');
			$menu = $('#menu'),
			$main = $('#main');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
				}, 100);
			});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

		//Hide Alerts
		$(document).ready(function(){
	    $(".alert_close").click(function(){
	      $('.alert').hide("linear");
	    });
	  });

		//Hide / Show Score table of profile view

		$(document).ready(function(){
	    $(".score-profile-btnup").click(function(){
				$('#score-profile').slideUp();
	    });
	  });

		$(document).ready(function(){
	    $(".score-profile-btndn").click(function(){
				$('#score-profile').slideDown();
	    });
	  });

		//typed development
    $(".typed").typed({
        strings: ["code", "idols", "community","Japan"],
        typeSpeed: 300,
        backDelay: 900,
        // loop
        loop: true
    });

		// Banners.
			$('#banner > article')
				.each(function() {

					var $this = $(this),
						$img = $this.children('img'),
						x;

					// Assign image.
						$this.css('background-image', 'url(' + $img.attr('src') + ')');

					// Set background position.
						if (x = $img.data('position'))
							$this.css('background-position', x);

					// Hide <img>.
						$img.hide();

				});

				// Menu.
					$menu
					.appendTo($body)
					.panel({
						delay: 500,
						hideOnClick: true,
						hideOnSwipe: true,
						resetScroll: true,
						resetForms: true,
						side: 'right',
						target: $body,
						visibleClass: 'is-menu-visible'
					})


			// Search (header).
				var $search = $('#search'),
					$search_input = $search.find('input');

					$body
					.on('click', '[href="#search"]', function(event) {

						event.preventDefault();

						// Not visible?
							if (!$search.hasClass('visible')) {

								// Reset form.
									$search[0].reset();

								// Show.
									$search.addClass('visible');

								// Focus input.
									$search_input.focus();

							}

					});

				$search_input
					.on('keydown', function(event) {

						if (event.keyCode == 27)
							$search_input.blur();

					})
					.on('blur', function() {
						window.setTimeout(function() {
							$search.removeClass('visible');
						}, 100);
					});

			// Fix: Remove transitions on WP<10 (poor/buggy performance).
				if (skel.vars.os == 'wp' && skel.vars.osVersion < 10)
					$('#navPanel')
						.css('transition', 'none');

	});

})(jQuery);
